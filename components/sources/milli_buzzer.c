#include "milli_buzzer.h"

buzzer_sounds_t buzzer_sounds = {
    .sound1 = {{8,8}, {3000,60}, {0,60}, {3100,40}, {0,60}, {3100,80}, {0,80}, {3400,60}, {0,80} },
    .sound2 = {{8,8}, {3200,60}, {0,60}, {3300,40}, {0,60}, {3300,80}, {0,80}, {2000,60}, {0,80} },
    .sound3 = {{8,8}, {3400,60}, {0,60}, {3300,40}, {0,60}, {3200,80}, {0,80}, {3100,60}, {0,80} }
};

//////////////////////////////////////////// config PWM properties
void buzzer_init(void)
{
    // Prepare and then apply the LEDC PWM timer configuration
    ledc_timer_config_t ledc_timer = {
        .speed_mode       = LEDC_MODE,
        .timer_num        = LEDC_TIMER,
        .duty_resolution  = LEDC_DUTY_RES,
        .freq_hz          = LEDC_FREQUENCY,  // Set output frequency at 5 kHz
        .clk_cfg          = LEDC_AUTO_CLK
    };
    ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

    // Prepare and then apply the LEDC PWM channel configuration
    ledc_channel_config_t ledc_channel = {
        .speed_mode     = LEDC_MODE,
        .channel        = LEDC_CHANNEL,
        .timer_sel      = LEDC_TIMER,
        .intr_type      = LEDC_INTR_DISABLE,
        .gpio_num       = LEDC_OUTPUT_IO,
        .duty           = 0, // Set duty to 0%  /*!< LEDC channel duty, the range of duty setting is [0, (2**duty_resolution)] */
        .hpoint         = 0
    };
    ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
}

//////////////////////////////////////////// generate sound
void make_sound(uint16_t note[][2])
{
    //uint16_t** note = (uint16_t*)pvParameters;
	uint8_t i=0;
	uint16_t size = note[0][0];
    printf("Note Started.\n");

        for(i = 0; i < size; i++)
        {
            printf("note is (%d,%d)\n", note[i+1][0], note[i+1][1]);
            if(note[i+1][0]!= 0)
            {	
                ESP_ERROR_CHECK(ledc_set_freq(LEDC_MODE, LEDC_TIMER, (uint32_t)note[i+1][0]));
                ESP_ERROR_CHECK(ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, 4000));
                ESP_ERROR_CHECK(ledc_update_duty(LEDC_MODE, LEDC_CHANNEL));
                vTaskDelay(note[i+1][1]);
            }
            else if(note[i+1][0]==0)
            {
                ESP_ERROR_CHECK(ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, 0));
                ESP_ERROR_CHECK(ledc_update_duty(LEDC_MODE, LEDC_CHANNEL));
                vTaskDelay(note[i+1][1]);
            }
        }  	
}

//////////////////////////////////////////// a Task for buzzer
void task_buzzer_receive(void* pvParameters)
{
    uint8_t rdata;
    BaseType_t num_message, receive_status;
    for(;;)
    {
        num_message = uxQueueMessagesWaiting(sound_qu);
        if(num_message == 0)
            {
                //printf("Queue is empty!\n");
            }
        else
        {
            receive_status = xQueueReceive(sound_qu, &rdata, 0);
            if(receive_status)
                {
                    printf("Data %d received successfuly!\n", rdata);
                    switch(rdata)
                    {
                        case 1: make_sound(buzzer_sounds.sound1); break; //NOT forget to use BREAK!
                        case 2: make_sound(buzzer_sounds.sound2); break;
                        case 3: make_sound(buzzer_sounds.sound3); break;
                        default : make_sound(buzzer_sounds.sound3); break;
                    }
                }
            else
                printf("Data NOT received!\n");
        }
    } 
}


