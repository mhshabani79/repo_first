#include "milli_adc.h"

static const char *TAG = "ADC DMA";


void bubble_sort(uint16_t* arr, uint8_t len)
{
	uint16_t i, j, temp;
	for(i=len; i>0; i--)
	{
		for(j=0; j<i-1; j++)
		{
			if(arr[j]>arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void continuous_adc_init(void)
{
    adc_digi_init_config_t adc_dma_config = {
        .max_store_buf_size = 32,   // ??
        .conv_num_each_intr = TIMES,
        .adc1_chan_mask = ADC1_CHAN_MASK,
        .adc2_chan_mask = ADC2_CHAN_MASK,
    };
    ESP_ERROR_CHECK(adc_digi_initialize(&adc_dma_config));

    adc_digi_configuration_t dig_cfg = {
        .conv_limit_en = ADC_CONV_LIMIT_EN, //??
        .conv_limit_num = 32, //250 //??
        .sample_freq_hz = 611,//SOC_ADC_SAMPLE_FREQ_THRES_LOW
        .conv_mode = ADC_CONV_MODE,
        .format = ADC_OUTPUT_TYPE,
    };

    adc_digi_pattern_config_t adc_pattern[SOC_ADC_PATT_LEN_MAX] = {0};
    dig_cfg.pattern_num = sizeof(CHANNEL) / sizeof(adc_channel_t);
    for (int i = 0; i < sizeof(CHANNEL) / sizeof(adc_channel_t); i++) {
        uint8_t unit = GET_UNIT(CHANNEL[i]);
        uint8_t ch = CHANNEL[i] & 0x7;
        adc_pattern[i].atten = ADC_ATTEN_DB_11;//ADC_ATTEN_DB_0;  
        adc_pattern[i].channel = ch;
        adc_pattern[i].unit = unit;
        adc_pattern[i].bit_width = SOC_ADC_DIGI_MAX_BITWIDTH;

        ESP_LOGI(TAG, "adc_pattern[%d].atten is :%x", i, adc_pattern[i].atten);
        ESP_LOGI(TAG, "adc_pattern[%d].channel is :%x", i, adc_pattern[i].channel);
        ESP_LOGI(TAG, "adc_pattern[%d].unit is :%x", i, adc_pattern[i].unit);
    }
    dig_cfg.adc_pattern = adc_pattern;
    ESP_ERROR_CHECK(adc_digi_controller_configure(&dig_cfg));
}

// #if !CONFIG_IDF_TARGET_ESP32
// static bool check_valid_data(const adc_digi_output_data_t *data) //??
// {
//     const unsigned int unit = data->type2.unit;
//     if (unit > 2) return false;
//     if (data->type2.channel >= SOC_ADC_CHANNEL_NUM(unit)) return false;

//     return true;
// }
// #endif

void task_adc_NTC_LDR(void *pvArg)
{
  esp_err_t ret;
  uint32_t ret_num = 0;
  uint16_t ntc[CH_SIZE];
  uint16_t ldr[CH_SIZE];
  uint8_t ntc_index=0;
  uint8_t ldr_index=0;
  adc_digi_output_data_t *p;
  uint8_t result[TIMES] = {0};
  for (;;)
  {
    vTaskDelay(5000); /*5 sec*/
    ret = adc_digi_read_bytes(result, TIMES, &ret_num, ADC_MAX_DELAY);
    if (ret == ESP_OK || ret == ESP_ERR_INVALID_STATE)
    {
      ESP_LOGI("TASK:", "ret is %x, ret_num is %d", ret, ret_num);
      ntc_index=0;
      ldr_index=0;
      for (int i = 0; i < ret_num; i += ADC_RESULT_BYTE)
      {
        p = (void *)&result[i];
        if (p->type1.channel == 2)
        {
          ntc[ntc_index++] = p->type1.data;
        }
        else if (p->type1.channel == 3)
        {
          ldr[ldr_index++] = p->type1.data;
        }
      }
      // get_temperature(ntc[4]); //add later
      // send ntc[4] / ldr[4]  to "Print_NTC_LDR" TASK, via semaphore,task notification,queue ....
      bubble_sort(ntc, 8);
      bubble_sort(ldr, 8);
      ESP_LOGI(TAG, " %d,  %d  ,%d,  %d,  %d,  %d,  %d,  %d", ntc[0], ntc[1], ntc[2], ntc[3], ntc[4], ntc[5], ntc[6], ntc[7]);
      ESP_LOGI(TAG, " %d,  %d  ,%d,  %d,  %d,  %d,  %d,  %d", ldr[0], ldr[1], ldr[2], ldr[3], ldr[4], ldr[5], ldr[6], ldr[7]);
      ESP_LOGI(TAG, "NTC VALUE= %d", ntc[CH_SIZE/2]);
      ESP_LOGI(TAG, "LDR VALUE= %d", ldr[CH_SIZE/2]);
    }
  }
    vTaskDelay(5000); /*5 sec*/
}
