#include "milli_i2c.h"

/////////////////////////////////////////////////////////// i2c Configuration
esp_err_t i2c_master_init(void)
{
    int i2c_master_port = 0;
    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,                //I2C_MODE_SLAVE // set the Master or Slave mode
        .sda_io_num = I2C_MASTER_SDA_IO,        //GPIO pin
        .sda_pullup_en = GPIO_PULLUP_ENABLE,    //GPIO_PULLUP_DISABLE
        .scl_io_num = I2C_MASTER_SCL_IO,        //GPIO pin
        .scl_pullup_en = GPIO_PULLUP_ENABLE,    //GPIO_PULLUP_DISABLE
        .master.clk_speed = I2C_MASTER_FREQ_HZ, //Master Frequency
        // .clk_flags = 0,          /*!< Optional, you can use I2C_SCLK_SRC_FLAG_* flags to choose i2c source clock here. */
    };
    esp_err_t err = i2c_param_config(i2c_master_port, &conf);
    if (err != ESP_OK)
    {
        return err;
    }
    return i2c_driver_install(i2c_master_port, conf.mode, 0, 0, 0);
}

/////////////////////////////////////////////////// Master will write to slave(s) via this func.
esp_err_t i2c_master_write_slave(uint8_t *data, size_t size)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, SLAVE_ADDR << 1, ACK_CHECK_EN);
    i2c_master_write(cmd, data, size, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(0, cmd, I2C_MASTER_TIMEOUT_MS / portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

/////////////////////////////////////////////////// i2c receiver task
void task_i2c_receive(void *pvarg)
{
    frame_t frame;
    uint8_t i;
    BaseType_t num_message, receive_status;
    for (;;)
    {
        num_message = uxQueueMessagesWaiting(i2c_qu);
        if (num_message == 0)
        {
            //printf("Queue is empty!\n");
        }
        else
        {
            receive_status = xQueueReceive(i2c_qu, &frame, 0);
            if (receive_status)
            {
                printf("Data i2c received successfuly!\n");
                if (!frame.delay)
                    for (i = 0; i < FRAME_NUMS; i++)
                    {
                        i2c_master_write_slave(frame.data[i * FRAME_SIZE], FRAME_SIZE);
                        vTaskDelay(FRAME_DELAY / portTICK_PERIOD_MS);
                    }
                else
                {
                    i2c_master_write_slave(frame.data, FRAME_SIZE * FRAME_NUMS);
                }
            }
            else
                printf("Data i2c NOT received!\n");
        }
    }
}

/////////////////////////////////////////////////// i2c sender task
void task_i2c_send(void *pvargs)
{
    frame_t frame;
    for (;;)
    {
        BaseType_t send_status = xQueueSendToBack(i2c_qu, &frame, 100);
        if (send_status)
            printf("i2c was sent!\n");
        else
            printf("i2c NOT sent!\n");

        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
