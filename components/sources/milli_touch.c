#include "milli_touch.h"

touch_state_t tstate = {
    .hold_cnt = {0},
    .release_state = {0},
};

/////////////////////////////////////////////////// Check if all array members are equal
bool check_array_equal(const int a[], int n)
{
    while (--n > 0 && a[n] == a[0])
        ;
    return n != 0;
}
/////////////////////////////////////////////////// initializing touch configs
void touch_init(void)
{
    touch_pad_init();
    for (int i = 0; i < TOUCH_NUM; i++)
    {
        touch_pad_config(BUTTON[i]);
    }
    vTaskDelay(100 / portTICK_RATE_MS);

#if IIR_FILTER_ENABLE
    touch_filter_config_t filter_info = {
        .mode = TOUCH_PAD_FILTER_IIR_8,
        .debounce_cnt = 1,
        .noise_thr = 0,
        .jitter_step = 4,
        .smh_lvl = TOUCH_PAD_SMOOTH_IIR_2,
    };
    touch_pad_filter_set_config(&filter_info);
    touch_pad_filter_enable();
    printf("IRR Filter enabled.\n");
#endif

#if DENOISE_ENABLE
    touch_pad_denoise_t denoise = {
        .grade = TOUCH_PAD_DENOISE_BIT4,
        .cap_level = TOUCH_PAD_DENOISE_CAP_L4,
    };
    touch_pad_denoise_set_config(&denoise);
    touch_pad_denoise_enable();
    printf("Denoise enabled.\n");
#endif

#if TOUCH_CHANGE_CONFIG
    touch_pad_set_meas_time(TOUCH_PAD_SLEEP_CYCLE_DEFAULT, TOUCH_PAD_MEASURE_CYCLE_DEFAULT);
    touch_pad_set_voltage(TOUCH_PAD_HIGH_VOLTAGE_THRESHOLD, TOUCH_PAD_LOW_VOLTAGE_THRESHOLD, TOUCH_PAD_ATTEN_VOLTAGE_THRESHOLD);
    touch_pad_set_idle_channel_connect(TOUCH_PAD_IDLE_CH_CONNECT_DEFAULT);
    for (int i = 0; i < TOUCH_BUTTON_NUM; i++)
    {
        touch_pad_set_cnt_mode(BUTTON[i], TOUCH_PAD_SLOPE_DEFAULT, TOUCH_PAD_TIE_OPT_DEFAULT);
    }
#endif

    touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
    touch_pad_fsm_start();
}

/////////////////////////////////////////////////// find offset manually or not
void find_offset(uint32_t *touch_bench)
{
    uint8_t i, j;
    uint32_t temp = 0;
#if AUTO_BENCHMARK_ENABLE
    for (i = 0; i < TOUCH_NUM; i++)
    {
        touch_pad_read_benchmark(BUTTON[i], &touch_bench[i]);
        printf("auto benchmark[%d] is : %d\n", i + 1, touch_bench[i]);
    }
#else
    for (i = 0; i < 25; i++)
    {
        for (j = 0; j < TOUCH_NUM; j++)
        {
            touch_pad_read_raw_data(BUTTON[j], &temp);
            touch_bench[j] = temp + touch_bench[j];
        }
        vTaskDelay(1);
    }
    for (i = 0; i < TOUCH_NUM; i++)
    {
        touch_bench[i] = touch_bench[i] / 100;
        printf("manual benchmark[%d] is : %d\n", i + 1, touch_bench[i]);
    }
#endif
}

/////////////////////////////////////////////////// task for reading touch
void task_touch(void *pvParameter)
{
    touch_sample_t tvars = {
        .sample_buff = {{0}},
        .sample_bench = {0},
        .sample_delta = {0},
        .sample_delta_flag = {0}};

    int8_t i, j;

    vTaskDelay(100 / portTICK_PERIOD_MS);

    find_offset(tvars.sample_bench);

    while (1)
    {
        for (i = 0; i < TOUCH_NUM; i++)
        {
            for (j = TOUCH_SAMPLE - 1; j > 0; j--)
            {
                tvars.sample_buff[i][j] = tvars.sample_buff[i][j - 1];
            }
            touch_pad_read_raw_data(BUTTON[i], &tvars.sample_buff[i][0]);

            for (j = 0; j < TOUCH_SAMPLE; j++)
            {
                tvars.sample_delta[j] = POSITIVE((int32_t)(tvars.sample_buff[i][j] - tvars.sample_bench[i]));

                if (tvars.sample_delta[j] >= tvars.sample_bench[i] * PUSH_LOWER_BOUND && tvars.sample_delta[j] < tvars.sample_bench[i] * PUSH_UPPER_BOUND)
                    tvars.sample_delta_flag[j] = TOUCH_STATE_PUSH;
                else if (tvars.sample_delta[j] >= tvars.sample_bench[i] * PUSH_UPPER_BOUND)
                    tvars.sample_delta_flag[j] = TOUCH_STATE_ABNORMAL;
                else if (tvars.sample_delta[j] < tvars.sample_bench[i] * PUSH_LOWER_BOUND)
                    tvars.sample_delta_flag[j] = TOUCH_STATE_RELEASE;
            }
            int n = TOUCH_SAMPLE;
            while (--n > 0 && tvars.sample_delta_flag[n] == tvars.sample_delta_flag[0])
            {
            }

            if (tstate.hold_cnt[i] > HOLD_MAX_COUNT)
            {
                tstate.hold_cnt[i] = 0;
            }

            if (n == 0)
            {
                if (tvars.sample_delta_flag[0] == TOUCH_STATE_PUSH)
                {
                    if (!tstate.release_state[i])
                        tstate.hold_cnt[i]++;
                    else if (tstate.release_state[i])
                        tstate.hold_cnt[i] = 1;
                    tstate.release_state[i] = 0;
                }
                else if (tvars.sample_delta_flag[0] == TOUCH_STATE_ABNORMAL)
                {
                    touch_pad_read_benchmark(BUTTON[i], &tvars.sample_bench[i]);
                    tstate.hold_cnt[i] = 0;
                    tstate.release_state[i] = 0;
                }
                else if (tvars.sample_delta_flag[0] == TOUCH_STATE_RELEASE)
                {
                    tstate.release_state[i] = 1;
                }
                else
                {
                }
            }
            printf("T%d: [%4d][%2d] ", BUTTON[i], tvars.sample_delta[TOUCH_SAMPLE - 1], tstate.hold_cnt[i]);
        }
        vTaskDelay(SAMPLE_DELAY / portTICK_PERIOD_MS);
        printf("\n");
    }
}