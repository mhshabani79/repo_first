#include "main.h"
#include "milli_buzzer.h"
#include "milli_adc.h"
#include "milli_touch.h"
#include "milli_i2c.h"
#include "milli_nvs.h"

static void sender_task_func(void *pvParameters)
{
    uint8_t sdata = 1;
    for (;;)
    {
        BaseType_t send_status = xQueueSendToBack(sound_qu, &sdata, 100);
        if (send_status)
            printf("Sound %d was sent!\n", sdata);
        else
            printf("Sound %d NOT sent!\n", sdata);
        sdata++;
        if (sdata == 4)
            sdata = 1;
        vTaskDelay(1000);
    }
}

static void task_print_NTC_LDR(void *pvArg)
{
    for (;;)
    {
        vTaskDelay(1000);
        printf("TASK print is Running\n");
    }
}

void app_main(void)
{
    // ////////////////////////ADC
    //     continuous_adc_init(); // optimize later
    //     adc_digi_start();
    //     xTaskCreate(task_adc_NTC_LDR, "ADC_NTC_LDR"      , 2048, NULL, 2, NULL);
    //     xTaskCreate(task_print_NTC_LDR, "print_NTC_LDR", 2048, NULL, 2, NULL);

    // ///////////////////////BUZZER
    //     buzzer_init();
    //     sound_qu = xQueueCreate(4, sizeof(uint8_t));
    //     if(sound_qu == NULL)
    //         {
    //             printf("Making Queue was failed!\n");
    //         }
    //     else
    //     {
    //         xTaskCreate(sender_task_func, "sender", 2048, NULL, 2, NULL);
    //         xTaskCreate(task_buzzer_receive, "buzzer", 2048, NULL, 2,NULL);
    //     }

    // ///////////////////////TOUCH
    // touch_init();
    // xTaskCreate(&task_touch, "touch_pad_task", 2048, NULL, 2, NULL);

    ///////////////////////I2C
    // i2c_master_init();
    // frame_t frame;
    // i2c_qu = xQueueCreate(2, sizeof(frame_t));
    // if(i2c_qu == NULL)
    //     {
    //         printf("Making i2c Queue was failed!\n");
    //     }
    // else
    // {
        // xTaskCreate(task_i2c_send, "sender_i2c", 2048, NULL, 2, NULL);
        // xTaskCreate(task_i2c_receive, "i2c", 2048, NULL, 2,NULL);
    // }

    // /////////////////////// NVS
    nvs_init();
    nvs_vars_t got = nvs_get_data();
    nvs_vars_t test = {10,20,30,-40,"salam"};
    nvs_update(test);

}