#include "milli_nvs.h"


void nvs_init(void)
{
    nvs_vars_t milli_vars = {
        .var1 = NVS_VAR1,
        .var2 = NVS_VAR2,
        .var3 = NVS_VAR3,
        .var4 = NVS_VAR4,
        .var5 = NVS_VAR5
    };
 
    
    //Initializing
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    //Opening
    err = nvs_open("milli_nvs", NVS_READWRITE, &handle1);
    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        printf("NVS handle opened!\n");

        nvs_set_u8(handle1, "var1", milli_vars.var1);
        nvs_set_u16(handle1, "var2", milli_vars.var2);
        nvs_set_u32(handle1, "var3", milli_vars.var3);
        nvs_set_i32(handle1, "var4", milli_vars.var4);
        nvs_set_str(handle1, "var5", milli_vars.var5);

        printf("Vars set.\n");
        printf("Committing vars in NVS ...");
        err = nvs_commit(handle1);
        printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
        nvs_close(handle1);
    }
}

nvs_vars_t nvs_get_data(void)
{
    nvs_vars_t get_data;

    //Initializing
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    //Opening
    err = nvs_open("milli_nvs", NVS_READWRITE, &handle1);
    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        uint8_t var1;
        uint16_t var2;
        uint32_t var3;
        int32_t var4;
        //char var5[64];

        err = nvs_get_u8(handle1, "var1", &get_data.var1);
        switch (err)
        {
        case ESP_OK:
            printf("var1 = %d\n", get_data.var1);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The var1 is not initialized yet!\n");
            break;
        default:
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        err = nvs_get_u16(handle1, "var2", &get_data.var2);
        switch (err)
        {
        case ESP_OK:
            printf("var2 = %d\n", get_data.var2);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The var2 is not initialized yet!\n");
            break;
        default:
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        err = nvs_get_u32(handle1, "var3", &get_data.var3);
        switch (err)
        {
        case ESP_OK:
            printf("var3 = %d\n", get_data.var3);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The var3 is not initialized yet!\n");
            break;
        default:
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        err = nvs_get_i32(handle1, "var4", &get_data.var4);
        switch (err)
        {
        case ESP_OK:
            printf("var4 = %d\n", get_data.var4);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The var4 is not initialized yet!\n");
            break;
        default:
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        size_t required_size;
        nvs_get_str(handle1, "var5", NULL, &required_size);
        //char* var5 = malloc(required_size);
        err = nvs_get_str(handle1, "var5", get_data.var5, &required_size);
        switch (err)
        {
        case ESP_OK:
            printf("var5 = %s\n", get_data.var5);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The var5 is not initialized yet!\n");
            break;
        default:
            printf("Error (%s) reading!\n", esp_err_to_name(err));
        }
    }
    return get_data;
}


void nvs_update(nvs_vars_t data)
{
    //Initializing
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    //Opening
    err = nvs_open("milli_nvs", NVS_READWRITE, &handle1);
    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        printf("NVS handle opened!\n");

        nvs_set_u8(handle1, "var1", data.var1);
        nvs_set_u16(handle1, "var2", data.var2);
        nvs_set_u32(handle1, "var3", data.var3);
        nvs_set_i32(handle1, "var4", data.var4);
        nvs_set_str(handle1, "var5", data.var5);

        printf("Vars updated.\n");
        printf("Committing vars in NVS ...");
        err = nvs_commit(handle1);
        printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
        nvs_close(handle1);
    }
}
