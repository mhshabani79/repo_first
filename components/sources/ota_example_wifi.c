#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "protocol_examples_common.h"
#include "errno.h"
#include "freertos/event_groups.h"
#include "sys/socket.h"
#include "netdb.h"
#include "esp_wifi.h"
// #include "wifi_h.h"

#ifndef CONFIG_LWIP_MAX_SOCKETS
#define CONFIG_LWIP_MAX_SOCKETS 10
#endif
#if CONFIG_EXAMPLE_CONNECT_WIFI
#include "esp_wifi.h"
#endif
#define INVALID_SOCK (-1)
#define YIELD_TO_ALL_MS 50
#define CONFIG_EXAMPLE_TCP_SERVER_BIND_ADDRESS "0.0.0.0"
#define CONFIG_EXAMPLE_TCP_SERVER_BIND_PORT "3344"

#define BUFFSIZE 4096
#define OTA_ACK 0xFD
#define PRINT_OTA_DATA 0 //set it to 1, if you wanna print the ota received bytes.

// int binary_file_length = 0;
// bool image_header_was_checked = false;
// static char ota_write_data[BUFFSIZE] = {0};
// esp_ota_handle_t update_handle = 0;
// const esp_partition_t *update_partition = NULL;
// const esp_partition_t *configured = NULL;
// const esp_partition_t *running = NULL;
static const char *OTA_TAG = "OTA";
esp_err_t err;

typedef struct
{
    uint32_t binary_file_length;
    bool image_header_was_checked;
 //   char ota_write_data[BUFFSIZE];
    esp_ota_handle_t update_handle;
    esp_partition_t *update_partition;
    esp_partition_t *configured;
    esp_partition_t *running;
    struct
    {
        bool ready_new;
        uint32_t size;
        bool complete;
    } firmware;
} ota_vars_t;

ota_vars_t ota_vars = {
    .binary_file_length = 0,
    .image_header_was_checked = false,
 //   .ota_write_data = {0},
    .update_handle = 0,
    .update_partition = NULL,
    .configured = NULL,
    .running = NULL,
    .firmware = {
        .ready_new = false,
        .size = 0,
        .complete = false}};

struct Txmessage
{
    int8_t sockID;
    uint16_t size;
    uint8_t sockData[BUFFSIZE];
}; //TxBuff;
struct Rxmessage
{
    int8_t sockID;
    uint16_t size;
    char sockData[BUFFSIZE];
}; // RxBuff;

// struct firmware_status
// {
//     bool ready_new;
//     uint32_t size;
//     bool complete;
// };

struct usernet
{
    uint8_t bssid_[6];
    char SSID_[32];
    char PASS_[32];
}; // WiFi;

void send2sock_Q(const int8_t sock_id, const char *data, const uint16_t data_size);

static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data);
static void send_data1(void *pvparam);
static void RxData_analyze(void *pvparam);

static const char *TAG = "Milli";

//struct firmware_status firmware;
struct Txmessage TxBuff;
struct Rxmessage RxBuff;
struct usernet WiFi; //{.bssid_=0,.SSID_="ddddd",.PASS_="aaaa"};

static xQueueHandle Q_Handler_tcp_send = NULL;
static xQueueHandle Q_Handler_tcp_recive = NULL;
static int sock[CONFIG_LWIP_MAX_SOCKETS - 1];
void wifi_init_temp(void);

///////////////////////////////////////////////////////////////////////// OTA
void ota_delete(void)
{
    esp_ota_abort(ota_vars.update_handle);
    ota_vars.firmware.ready_new = false;
    ota_vars.firmware.complete = false;
    ota_vars.firmware.size = 0;
    ota_vars.binary_file_length = 0;
    ota_vars.image_header_was_checked = false;
    ota_vars.update_partition = NULL;
    ota_vars.configured = NULL;
    ota_vars.running = NULL;
}

void ota_make_handlers(void)
{
    ota_vars.configured = esp_ota_get_boot_partition();
    ota_vars.running = esp_ota_get_running_partition();

    if (ota_vars.configured != ota_vars.running)
    {
        ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
                 ota_vars.configured->address, ota_vars.running->address);
        ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    ESP_LOGI(OTA_TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             ota_vars.running->type, ota_vars.running->subtype, ota_vars.running->address);

    ota_vars.update_partition = esp_ota_get_next_update_partition(NULL);
    assert(ota_vars.update_partition != NULL);
    ESP_LOGI(OTA_TAG, "Writing to partition subtype %d at offset 0x%x",
             ota_vars.update_partition->subtype, ota_vars.update_partition->address);
}

void ota_function(char ota_write_data[BUFFSIZE],uint16_t data_read)
{
    if (ota_vars.firmware.ready_new == true)
    {
        if (strncmp(ota_write_data, "ENDOTA", 6) == 0)
        {
            ota_vars.firmware.complete = true;
            ESP_LOGI(OTA_TAG, "Firmware completed.");
        }
        else
        {
            ota_vars.firmware.complete = false;
            if (ota_vars.binary_file_length == ota_vars.firmware.size)
            {
                ESP_LOGE(OTA_TAG, "'ENDOTA' flag not found!");
                ota_vars.firmware.ready_new = false;
                return;
            }
        }
        if (ota_vars.binary_file_length < ota_vars.firmware.size)
        {
            if (ota_vars.image_header_was_checked == false)
            {
                if (data_read <= sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t) + sizeof(esp_app_desc_t))
                {
                    ESP_LOGE(OTA_TAG, "received package size is lower than headers!");
                    ota_delete();
                    return;
                }
                else
                {
                    esp_app_desc_t new_app_info;
                    memcpy(&new_app_info, &ota_write_data[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
                    ESP_LOGI(OTA_TAG, "New firmware version: %s", new_app_info.version);

                    esp_app_desc_t running_app_info;
                    if (esp_ota_get_partition_description(ota_vars.running, &running_app_info) == ESP_OK)
                    {
                        ESP_LOGI(OTA_TAG, "Running firmware version: %s", running_app_info.version);
                    }

                    const esp_partition_t *last_invalid_app = esp_ota_get_last_invalid_partition();
                    esp_app_desc_t invalid_app_info;
                    if (esp_ota_get_partition_description(last_invalid_app, &invalid_app_info) == ESP_OK)
                    {
                        ESP_LOGI(OTA_TAG, "Last invalid firmware version: %s", invalid_app_info.version);
                    }

                    // check current version with last invalid partition
                    if (last_invalid_app != NULL)
                    {
                        if (memcmp(invalid_app_info.version, new_app_info.version, sizeof(new_app_info.version)) == 0)
                        {
                            ESP_LOGW(OTA_TAG, "New version is the same as invalid version.");
                            ota_delete();
                            return;
                        }
                    }

                    // check current version with the Running version
                    if (memcmp(new_app_info.version, running_app_info.version, sizeof(new_app_info.version)) == 0)
                    {
                        ESP_LOGW(OTA_TAG, "Current running version is the same as a new. We will not continue the update.");
                        ota_delete();
                        return;
                    }

                    ota_vars.image_header_was_checked = true;

                    err = esp_ota_begin(ota_vars.update_partition, OTA_WITH_SEQUENTIAL_WRITES, &ota_vars.update_handle);
                    if (err != ESP_OK)
                    {
                        ESP_LOGE(OTA_TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
                        ota_delete();
                        return;
                    }
                    ESP_LOGI(OTA_TAG, "esp_ota_begin succeeded");
                }
            }
            ESP_LOGI(OTA_TAG, "Received LEN: %d", data_read);
#if PRINT_OTA_DATA == 1
            printf("DATA:");
            int i = 0;
            for (i = 0; i < BUFFSIZE; i++)
                printf("%X ", ota_write_data[i]);
            printf("\n");
#endif
            ////////////////////////////////////////////////////////////////////////

            err = esp_ota_write(ota_vars.update_handle, (const void *)ota_write_data, data_read);
            if (err != ESP_OK)
            {
                ota_delete();
                return;
            }
            /******************************************** Send ack 0xFD to client*/
            TxBuff.sockID = RxBuff.sockID;
            TxBuff.sockData[0] = OTA_ACK;
            TxBuff.size = 1; //sizeof(OTA_ACK);
            xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
            /********************************************/

            ota_vars.binary_file_length += data_read;
            ESP_LOGI(OTA_TAG, "Written image length %d", ota_vars.binary_file_length);
        }
        if (ota_vars.binary_file_length == ota_vars.firmware.size && ota_vars.firmware.complete == true)
        {
            /******************************************** Send Finish ack 0xFD to client*/
            TxBuff.sockID = RxBuff.sockID;
            TxBuff.sockData[0] = OTA_ACK;
            TxBuff.size = 1; //sizeof(OTA_ACK);
            xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
            /********************************************/

            ESP_LOGI(OTA_TAG, "Total Write binary data length: %d", ota_vars.binary_file_length);
            err = esp_ota_end(ota_vars.update_handle);
            if (err != ESP_OK)
            {
                if (err == ESP_ERR_OTA_VALIDATE_FAILED)
                {
                    ESP_LOGE(OTA_TAG, "Image validation failed, image is corrupted");
                    ota_delete();
                    return;
                }
                else
                {
                    ESP_LOGE(OTA_TAG, "esp_ota_end failed (%s)!", esp_err_to_name(err));
                    ota_delete();
                    return;
                }
            }

            err = esp_ota_set_boot_partition(ota_vars.update_partition);
            if (err != ESP_OK)
            {
                ESP_LOGE(OTA_TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
                ota_delete();
                return;
            }

            ESP_LOGI(OTA_TAG, "Prepare to restart system!");
            esp_restart();
        }
        else if (ota_vars.binary_file_length < ota_vars.firmware.size && ota_vars.firmware.complete == true)
        {
            ESP_LOGE(OTA_TAG, "OTA Bytes may missing!");
            ota_delete();
            return;
        }
    }
}

void app_main(void)
{
    //esp_ota_img_states_t ota_state;

    // Initialize NVS.
    err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
    wifi_init_temp();
}

////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

static void log_socket_error(const char *tag, const int sock, const int err, const char *message)
{
    ESP_LOGE(tag, "[sock=%d]: %s\n"
                  "error=%d: %s",
             sock, message, err, strerror(err));
}

static int try_receive(const char *tag, const int sock, char *data, size_t max_len)
{
    int len = recv(sock, data, max_len, 0);
    if (len < 0)
    {
        if (errno == EINPROGRESS || errno == EAGAIN || errno == EWOULDBLOCK)
        {
            return 0; // Not an error
        }
        if (errno == ENOTCONN)
        {
            ESP_LOGW(tag, "[sock=%d]: Connection closed", sock);
            return -2; // Socket has been disconnected
        }
        log_socket_error(tag, sock, errno, "Error occurred during receiving");
        return -1;
    }

    return len;
}

static int socket_send(const char *tag, const int sock, const char *data, const size_t len)
{
    int to_write = len;
    while (to_write > 0)
    {
        int written = send(sock, data + (len - to_write), to_write, 0);
        if (written < 0 && errno != EINPROGRESS && errno != EAGAIN && errno != EWOULDBLOCK)
        {
            log_socket_error(tag, sock, errno, "Error occurred during sending");
            return -1;
        }
        to_write -= written;
    }
    return len;
}

/**
 * @brief Returns the string representation of client's address (accepted on this server)
 */
static inline char *get_clients_address(struct sockaddr_storage *source_addr)
{
    static char address_str[128];
    char *res = NULL;
    // Convert ip address to string
    if (source_addr->ss_family == PF_INET)
    {
        res = inet_ntoa_r(((struct sockaddr_in *)source_addr)->sin_addr, address_str, sizeof(address_str) - 1);
    }
#ifdef CONFIG_LWIP_IPV6
    else if (source_addr->ss_family == PF_INET6)
    {
        res = inet6_ntoa_r(((struct sockaddr_in6 *)source_addr)->sin6_addr, address_str, sizeof(address_str) - 1);
    }
#endif
    if (!res)
    {
        address_str[0] = '\0'; // Returns empty string if conversion didn't succeed
    }
    return address_str;
}

static void tcp_server_task(void *pvParameters)
{
    // static char rx_buffer[128];
    static const char *TAG = "nonblocking-socket-server";
    struct addrinfo hints = {.ai_socktype = SOCK_STREAM};
    struct addrinfo *address_info;
    int listen_sock = INVALID_SOCK;
    const size_t max_socks = CONFIG_LWIP_MAX_SOCKETS - 1;
    struct Txmessage tx_buff_recived;
    // static int sock[CONFIG_LWIP_MAX_SOCKETS - 1];

    // Prepare a list of file descriptors to hold client's sockets, mark all of them as invalid, i.e. available
    for (int i = 0; i < max_socks; ++i)
    {
        sock[i] = INVALID_SOCK;
    }

    // Translating the hostname or a string representation of an IP to address_info
    int res = getaddrinfo(CONFIG_EXAMPLE_TCP_SERVER_BIND_ADDRESS, CONFIG_EXAMPLE_TCP_SERVER_BIND_PORT, &hints, &address_info);
    if (res != 0 || address_info == NULL)
    {
        ESP_LOGE(TAG, "couldn't get hostname for `%s` "
                      "getaddrinfo() returns %d, addrinfo=%p",
                 CONFIG_EXAMPLE_TCP_SERVER_BIND_ADDRESS, res, address_info);
        goto error;
    }

    // Creating a listener socket
    listen_sock = socket(address_info->ai_family, address_info->ai_socktype, address_info->ai_protocol);

    if (listen_sock < 0)
    {
        log_socket_error(TAG, listen_sock, errno, "Unable to create socket");
        goto error;
    }
    ESP_LOGI(TAG, "Listener socket created");

    // Marking the socket as non-blocking
    int flags = fcntl(listen_sock, F_GETFL);
    if (fcntl(listen_sock, F_SETFL, flags | O_NONBLOCK) == -1)
    {
        log_socket_error(TAG, listen_sock, errno, "Unable to set socket non blocking");
        goto error;
    }
    ESP_LOGI(TAG, "Socket marked as non blocking");

    // Binding socket to the given address
    int err = bind(listen_sock, address_info->ai_addr, address_info->ai_addrlen);
    if (err != 0)
    {
        log_socket_error(TAG, listen_sock, errno, "Socket unable to bind");
        goto error;
    }
    ESP_LOGI(TAG, "Socket bound on %s:%s", CONFIG_EXAMPLE_TCP_SERVER_BIND_ADDRESS, CONFIG_EXAMPLE_TCP_SERVER_BIND_PORT);

    // Set queue (backlog) of pending connections to one (can be more)
    err = listen(listen_sock, 1);
    if (err != 0)
    {
        log_socket_error(TAG, listen_sock, errno, "Error occurred during listen");
        goto error;
    }
    ESP_LOGI(TAG, "Socket listening");
    // xSemaphoreGive(*server_ready);

    // Main loop for accepting new connections and serving all connected clients
    while (1)
    {
        struct sockaddr_storage source_addr; // Large enough for both IPv4 or IPv6
        socklen_t addr_len = sizeof(source_addr);

        // Find a free socket
        int new_sock_index = 0;
        for (new_sock_index = 0; new_sock_index < max_socks; ++new_sock_index)
        {
            if (sock[new_sock_index] == INVALID_SOCK)
            {
                break;
            }
        }

        // We accept a new connection only if we have a free socket
        if (new_sock_index < max_socks)
        {
            // Try to accept a new connections
            sock[new_sock_index] = accept(listen_sock, (struct sockaddr *)&source_addr, &addr_len);

            if (sock[new_sock_index] < 0)
            {
                if (errno == EWOULDBLOCK)
                { // The listener socket did not accepts any connection
                    // continue to serve open connections and try to accept again upon the next iteration
                    ESP_LOGV(TAG, "No pending connections...");
                }
                else
                {
                    log_socket_error(TAG, listen_sock, errno, "Error when accepting connection");
                    goto error;
                }
            }
            else
            {
                // We have a new client connected -> print it's address
                ESP_LOGI(TAG, "[sock=%d]: Connection accepted from IP:%s", sock[new_sock_index], get_clients_address(&source_addr));

                // ...and set the client's socket non-blocking
                flags = fcntl(sock[new_sock_index], F_GETFL);
                if (fcntl(sock[new_sock_index], F_SETFL, flags | O_NONBLOCK) == -1)
                {
                    log_socket_error(TAG, sock[new_sock_index], errno, "Unable to set socket non blocking");
                    goto error;
                }
                ESP_LOGI(TAG, "[sock=%d]: Socket marked as non blocking", sock[new_sock_index]);
            }
        }

        // We serve all the connected clients in this loop
        for (int i = 0; i < max_socks; ++i)
        {
            if (sock[i] != INVALID_SOCK)
            {

                // This is an open socket -> try to serve it
                int len = try_receive(TAG, sock[i], RxBuff.sockData, sizeof(RxBuff.sockData));
                if (len < 0)
                {
                    // Error occurred within this client's socket -> close and mark invalid
                    ESP_LOGI(TAG, "[sock=%d]: try_receive() returned %d -> closing the socket", sock[i], len);
                    close(sock[i]);
                    sock[i] = INVALID_SOCK;
                }
                else if (len > 0)
                {
                    // memcpy(RxBuff.sockData, rx_buffer, 20);
                    RxBuff.size = len;
                    RxBuff.sockID = i;
                    xQueueSend(Q_Handler_tcp_recive, (void *)&RxBuff, NULL);
                    // Received some data -> echo back
                    // ESP_LOGI(TAG, "[sock=%d]: Received %.*s", sock[i], len, RxBuff.sockData);
                }

            } // one client's socket
        }     // for all sockets

        { // wait for data to be send queue
            while (xQueueReceive(Q_Handler_tcp_send, &tx_buff_recived, 0))
            {
                if (tx_buff_recived.sockID != INVALID_SOCK)
                {
                    /*len = */ socket_send(TAG, sock[tx_buff_recived.sockID] /*sock[i]*/, (char *)tx_buff_recived.sockData, tx_buff_recived.size);
                }
            }
        }
        // Yield to other tasks
        vTaskDelay(pdMS_TO_TICKS(YIELD_TO_ALL_MS)); //!!
        //vTaskDelay(pdMS_TO_TICKS(5));
    }

error:
    if (listen_sock != INVALID_SOCK)
    {
        close(listen_sock);
    }

    for (int i = 0; i < max_socks; ++i)
    {
        if (sock[i] != INVALID_SOCK)
        {
            close(sock[i]);
        }
    }

    free(address_info);
    vTaskDelete(NULL);
}
void init_wifi_station(void)
{
    wifi_config_t wifi_config = {
        .sta = {
            //.ssid = WiFi.SSID_[0],//"MilliGrade.ir",
            //.password = "MilliGrade.ir",//WiFi.PASS_,//"Milli_!@#",
            .pmf_cfg = {
                .capable = true,
                .required = false},
        },
    };

    memcpy(wifi_config.sta.ssid, WiFi.SSID_, strlen(WiFi.SSID_));
    memcpy(wifi_config.sta.password, WiFi.PASS_, strlen(WiFi.PASS_));

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(TAG, " Station config finished.");
}

void wifi_init_temp(void)
{
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    /*fech eeprom wifi data (last SSID and PASS) */
    strcpy(WiFi.SSID_, "MilliGrade.ir");
    strcpy(WiFi.PASS_, "Milli_!@#");

    // ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    // ESP_ERROR_CHECK(esp_wifi_start());
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    init_wifi_station();

    Q_Handler_tcp_send = xQueueCreate(10, sizeof(TxBuff));
    Q_Handler_tcp_recive = xQueueCreate(10, sizeof(RxBuff));
    // xTaskCreate(send_data1, "send_data1", 2048, NULL, 5, NULL); // alaki . send to queue
    xTaskCreate(RxData_analyze, "RxData_analyze", BUFFSIZE * 2, NULL, 5, NULL);
    xTaskCreate(tcp_server_task, "tcp_server", 8192, NULL /*&server_ready*/, 5, NULL);
}

void send2sock_Q(const int8_t sock_id, const char *data, const uint16_t data_size)
{
    TxBuff.sockID = sock_id;
    memcpy(TxBuff.sockData, data, sizeof(data_size));
    TxBuff.size = data_size;
    xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
}

/*send_data1 for test*/
static void send_data1(void *pvparam)
{
    while (1)
    {
        // uint32_t heap_size = heap_caps_get_minimum_free_size(MALLOC_CAP_DEFAULT);
        // ESP_LOGI(TAG, "min heap size: %u", heap_size);
        vTaskDelay(200);
        for (int i = 0; i < 9; i++)
        {
            if (sock[i] != INVALID_SOCK)
            {
                memset(TxBuff.sockData, 0x00, sizeof(TxBuff.sockData));
                memset(TxBuff.sockData, 0x30 + i, 10);
                TxBuff.sockID = i;
                // ESP_LOGI("jj", "No %d.",i);
                xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
            }
            // send queue
        }
    }
}
static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT)
    {
        switch (event_id)
        {
            { /*AP EVENT*/
            case WIFI_EVENT_AP_STACONNECTED:
            {
                wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
                ESP_LOGI(TAG, "station " MACSTR " join, AID=%d",
                         MAC2STR(event->mac), event->aid);
            }
            break;
            case WIFI_EVENT_AP_STADISCONNECTED:
            {
                wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
                ESP_LOGI(TAG, "station " MACSTR " leave, AID=%d",
                         MAC2STR(event->mac), event->aid);
                // init_wifi_station();
            }
            break;
            case WIFI_EVENT_AP_START:
            {
                ESP_LOGI(TAG, "***AP START+++");
            }
            break;
            case WIFI_EVENT_AP_STOP:
            {
                ESP_LOGI(TAG, "***AP STOP+++");
            }
            break;
            case WIFI_EVENT_AP_PROBEREQRECVED:
            {
                ESP_LOGI(TAG, "***AP PROBEREQRECVED+++");
            }
            break;
            }
            { /*STATION EVENT*/
            case WIFI_EVENT_STA_CONNECTED:
            {
                // wifi_event_sta_connected_t
                ESP_LOGI(TAG, "***STA CONNECTED+++");
            }
            break;
            case WIFI_EVENT_STA_DISCONNECTED:
            {
                wifi_event_sta_disconnected_t *dis_event = (wifi_event_sta_disconnected_t *)event_data;
                ESP_LOGI(TAG, "***STA DISC Reason: %d ", dis_event->reason);
                if (1) // if recoonect == 1
                {
                    vTaskDelay(200);
                    esp_wifi_connect();
                    /*
            reconnect_cnt++;
            if reconnect++>10 scan ssid
          */
                }
            }
            break;
            case WIFI_EVENT_SCAN_DONE: /**< ESP32 finish scanning AP */
            {
                ESP_LOGI(TAG, "***SCAN MODE+++");
                //
            }
            break;
            case WIFI_EVENT_STA_START:
            {
                ESP_LOGI(TAG, "***STA START+++");
                esp_wifi_connect();
            }
            break;
            case WIFI_EVENT_STA_STOP:
            {
                ESP_LOGI(TAG, "***STA STOP+++");
            }
            break;
            case WIFI_EVENT_WIFI_READY:
            {
                /*not important, This event may be removed in future releases.*/
                ESP_LOGI(TAG, "***WIFI READY+++");
            }
            break;
            }
        default:
        {
        }
        break;
            //}
        }
    }
    else if (event_base == IP_EVENT)
    {
        switch (event_id)
        {
        case IP_EVENT_STA_GOT_IP:
        {
            ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
            ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
            // s_retry_num = 0;
            // xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);

            break;
        }
        }
    }
}

static void RxData_analyze(void *pvparam)
{
    struct Rxmessage rx_buff;

    while (1)
    {
        // vTaskDelay(100);

        while (xQueueReceive(Q_Handler_tcp_recive, &rx_buff, 100))
        {
            if (rx_buff.sockID != INVALID_SOCK)
            {
                if (strncmp(rx_buff.sockData, "SSIDLIST", 8) == 0)
                {
                    /* get available access points*/
                }
                else if (strncmp(rx_buff.sockData, "SSIDPASS", 8) == 0)
                {
                    /*connect to AP*/
                    /*for test*/
                    wifi_config_t wifi_config = {
                        .sta = {
                            .ssid = "AndroidAP",    //"MilliGrade.ir",
                            .password = "12345678", // WiFi.PASS_,//"Milli_!@#",
                            .pmf_cfg = {
                                .capable = true,
                                .required = false},
                        },
                    };
                    esp_wifi_disconnect();
                    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
                    ESP_ERROR_CHECK(esp_wifi_start());
                    esp_wifi_connect();
                    ESP_LOGI(TAG, "ssidpassss ");
                }
                else if (strncmp(rx_buff.sockData, "GETID", 5) == 0)
                {
                    /*send device id */
                    TxBuff.sockID = rx_buff.sockID;
                    memcpy(TxBuff.sockData, "milli8765", 10);
                    TxBuff.size = strlen((char *)TxBuff.sockData);

                    xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, 2);
                    // ipd_message=IPD_GET_ID;
                }
                else if (strncmp(rx_buff.sockData, "S=", 2) == 0)
                {
                    /*set satus*/
                }
                else if (strncmp(rx_buff.sockData, "S?", 2) == 0)
                {
                    /*get status*/
                }
                else if (strncmp(rx_buff.sockData, "P=", 2) == 0)
                {
                    /*set parameters*/
                }
                else if (strncmp(rx_buff.sockData, "P?", 2) == 0)
                {
                    /*get parameters*/
                }
                else if (strncmp(rx_buff.sockData, "Heap", 4) == 0) /*Get available Free heap size in Bytes*/
                {
                    uint32_t heap_size = heap_caps_get_minimum_free_size(MALLOC_CAP_DEFAULT);
                    char str_temp[30];
                    memset(str_temp, 0x00, sizeof(str_temp));
                    sprintf(str_temp, "min heap size: %u", heap_size);
                    uint8_t str_size = strlen(str_temp);

                    TxBuff.sockID = rx_buff.sockID;
                    memcpy(TxBuff.sockData, str_temp, str_size);
                    TxBuff.size = str_size; // strlen((char *)TxBuff.sockData);
                    xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, 2);
                }
                else if (strncmp(rx_buff.sockData, "NEWFW,", 6) == 0)
                {
                    uint32_t fw_size = (uint32_t)atoi(rx_buff.sockData + 6);
                    if (fw_size > 1)
                    {
                        ota_vars.firmware.ready_new = true;
                        ota_vars.firmware.size = fw_size;
                        ota_make_handlers();
                        TxBuff.sockID = RxBuff.sockID;
                        TxBuff.sockData[0] = OTA_ACK;
                        TxBuff.size = 1; //sizeof(OTA_ACK);
                        xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
                    }
                    else
                    {
                        ota_vars.firmware.ready_new = false;
                    }
                    ESP_LOGI(OTA_TAG, "Frameware size : %d Bytes", fw_size);
                } 
                else
                {
                    ota_function(rx_buff.sockData, rx_buff.size);
                }
            }
        }
    }
}
