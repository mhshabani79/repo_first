#include "milli_ota.h"

ota_vars_t ota_vars = {
    .binary_file_length = 0,
    .image_header_was_checked = false,
 //   .ota_write_data = {0},
    .update_handle = 0,
    .update_partition = NULL,
    .configured = NULL,
    .running = NULL,
    .firmware = {
        .ready_new = false,
        .size = 0,
        .complete = false}};

//////////////////////////////////// Clear the ota variables, if canceled
void ota_delete(void)
{
    esp_ota_abort(ota_vars.update_handle);
    ota_vars.firmware.ready_new = false;
    ota_vars.firmware.complete = false;
    ota_vars.firmware.size = 0;
    ota_vars.binary_file_length = 0;
    ota_vars.image_header_was_checked = false;
    ota_vars.update_partition = NULL;
    ota_vars.configured = NULL;
    ota_vars.running = NULL;
}

//////////////////////////////////// Initialize some partition handlers for starting up the OTA
void ota_make_handlers(void)
{
    ota_vars.configured = esp_ota_get_boot_partition();
    ota_vars.running = esp_ota_get_running_partition();

    if (ota_vars.configured != ota_vars.running)
    {
        ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
                 ota_vars.configured->address, ota_vars.running->address);
        ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    ESP_LOGI(OTA_TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             ota_vars.running->type, ota_vars.running->subtype, ota_vars.running->address);

    ota_vars.update_partition = esp_ota_get_next_update_partition(NULL);
    assert(ota_vars.update_partition != NULL);
    ESP_LOGI(OTA_TAG, "Writing to partition subtype %d at offset 0x%x",
             ota_vars.update_partition->subtype, ota_vars.update_partition->address);
}

//////////////////////////////////// Perform OTA function. must call in the RxData_analyze task!
void ota_function(char ota_write_data[BUFFSIZE],uint16_t data_read)
{
    if (ota_vars.firmware.ready_new == true)
    {
        if (strncmp(ota_write_data, "ENDOTA", 6) == 0)
        {
            ota_vars.firmware.complete = true;
            ESP_LOGI(OTA_TAG, "Firmware completed.");
        }
        else
        {
            ota_vars.firmware.complete = false;
            if (ota_vars.binary_file_length == ota_vars.firmware.size)
            {
                ESP_LOGE(OTA_TAG, "'ENDOTA' flag not found!");
                ota_vars.firmware.ready_new = false;
                return;
            }
        }
        if (ota_vars.binary_file_length < ota_vars.firmware.size)
        {
            if (ota_vars.image_header_was_checked == false)
            {
                if (data_read <= sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t) + sizeof(esp_app_desc_t))
                {
                    ESP_LOGE(OTA_TAG, "received package size is lower than headers!");
                    ota_delete();
                    return;
                }
                else
                {
                    esp_app_desc_t new_app_info;
                    memcpy(&new_app_info, &ota_write_data[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
                    ESP_LOGI(OTA_TAG, "New firmware version: %s", new_app_info.version);

                    esp_app_desc_t running_app_info;
                    if (esp_ota_get_partition_description(ota_vars.running, &running_app_info) == ESP_OK)
                    {
                        ESP_LOGI(OTA_TAG, "Running firmware version: %s", running_app_info.version);
                    }

                    const esp_partition_t *last_invalid_app = esp_ota_get_last_invalid_partition();
                    esp_app_desc_t invalid_app_info;
                    if (esp_ota_get_partition_description(last_invalid_app, &invalid_app_info) == ESP_OK)
                    {
                        ESP_LOGI(OTA_TAG, "Last invalid firmware version: %s", invalid_app_info.version);
                    }

                    // check current version with last invalid partition
                    if (last_invalid_app != NULL)
                    {
                        if (memcmp(invalid_app_info.version, new_app_info.version, sizeof(new_app_info.version)) == 0)
                        {
                            ESP_LOGW(OTA_TAG, "New version is the same as invalid version.");
                            ota_delete();
                            return;
                        }
                    }

                    // check current version with the Running version
                    if (memcmp(new_app_info.version, running_app_info.version, sizeof(new_app_info.version)) == 0)
                    {
                        ESP_LOGW(OTA_TAG, "Current running version is the same as a new. We will not continue the update.");
                        ota_delete();
                        return;
                    }

                    ota_vars.image_header_was_checked = true;

                    err = esp_ota_begin(ota_vars.update_partition, OTA_WITH_SEQUENTIAL_WRITES, &ota_vars.update_handle);
                    if (err != ESP_OK)
                    {
                        ESP_LOGE(OTA_TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
                        ota_delete();
                        return;
                    }
                    ESP_LOGI(OTA_TAG, "esp_ota_begin succeeded");
                }
            }
            ESP_LOGI(OTA_TAG, "Received LEN: %d", data_read);
#if PRINT_OTA_DATA == 1
            printf("DATA:");
            int i = 0;
            for (i = 0; i < BUFFSIZE; i++)
                printf("%X ", ota_write_data[i]);
            printf("\n");
#endif
            ////////////////////////////////////////////////////////////////////////

            err = esp_ota_write(ota_vars.update_handle, (const void *)ota_write_data, data_read);
            if (err != ESP_OK)
            {
                ota_delete();
                return;
            }
            /******************************************** Send ack 0xFD to client*/
            TxBuff.sockID = RxBuff.sockID;  //TxBuff is defined by wifi component
            TxBuff.sockData[0] = OTA_ACK;
            TxBuff.size = 1; //sizeof(OTA_ACK);
            xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
            /********************************************/

            ota_vars.binary_file_length += data_read;
            ESP_LOGI(OTA_TAG, "Written image length %d", ota_vars.binary_file_length);
        }
        if (ota_vars.binary_file_length == ota_vars.firmware.size && ota_vars.firmware.complete == true)
        {
            /******************************************** Send Finish ack 0xFD to client*/
            TxBuff.sockID = RxBuff.sockID;
            TxBuff.sockData[0] = OTA_ACK;
            TxBuff.size = 1; //sizeof(OTA_ACK);
            xQueueSend(Q_Handler_tcp_send, (void *)&TxBuff, NULL);
            /********************************************/

            ESP_LOGI(OTA_TAG, "Total Write binary data length: %d", ota_vars.binary_file_length);
            err = esp_ota_end(ota_vars.update_handle);
            if (err != ESP_OK)
            {
                if (err == ESP_ERR_OTA_VALIDATE_FAILED)
                {
                    ESP_LOGE(OTA_TAG, "Image validation failed, image is corrupted");
                    ota_delete();
                    return;
                }
                else
                {
                    ESP_LOGE(OTA_TAG, "esp_ota_end failed (%s)!", esp_err_to_name(err));
                    ota_delete();
                    return;
                }
            }

            err = esp_ota_set_boot_partition(ota_vars.update_partition);
            if (err != ESP_OK)
            {
                ESP_LOGE(OTA_TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
                ota_delete();
                return;
            }

            ESP_LOGI(OTA_TAG, "Prepare to restart system!");
            esp_restart();
        }
        else if (ota_vars.binary_file_length < ota_vars.firmware.size && ota_vars.firmware.complete == true)
        {
            ESP_LOGE(OTA_TAG, "OTA Bytes may missing!");
            ota_delete();
            return;
        }
    }
}