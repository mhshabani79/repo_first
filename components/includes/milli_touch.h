#pragma once

#ifndef milli_touch_h
#define milli_touch_h

#ifdef __cplusplus
extern "C"
{
#endif

//Includes
#include <stdio.h>
#include "main.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/touch_pad.h"
#include "esp_log.h"

//Defines
#define DENOISE_ENABLE 1        //whether the Denoising is enabled or not.
#define IIR_FILTER_ENABLE 1     //whether the IIR filter is enabled or not.
#define AUTO_BENCHMARK_ENABLE 1 //whether use auto benchmark or manual function.
#define TOUCH_NUM 6             //How many touch pads are there?
#define TOUCH_SAMPLE 2          //how many samples for each read?
#define SAMPLE_DELAY 50         //the delay between each sampling in mS
#define HOLD_MAX_COUNT 50       //the maximmum amounts of holding counts
#define PUSH_LOWER_BOUND 0.03   //PUSH_LOWER_BOUND*Bench_mark = the minimmum amount for push
#define PUSH_UPPER_BOUND 0.20   //PUSH_UPPER_BOUND*Bench_mark = the maximmum amount for push
#define TOUCH_CHANGE_CONFIG 0
#define POSITIVE(X) ((X) < (0) ? (0) : (X))
#define BUTTON \
    (touch_pad_t[]) { TOUCH_PAD_NUM1, TOUCH_PAD_NUM2, TOUCH_PAD_NUM3, TOUCH_PAD_NUM4, TOUCH_PAD_NUM5, TOUCH_PAD_NUM6 }

    //Vars
    typedef struct
    {
        uint32_t sample_buff[TOUCH_NUM][TOUCH_SAMPLE]; //it hold the raw sample(s) of each touch number.
        uint32_t sample_bench[TOUCH_NUM];              //it hold the offset of each touch sensor for benchmarking.
        uint32_t sample_delta[TOUCH_SAMPLE];           //it hold raw_data - offset for each sample.
        uint8_t sample_delta_flag[TOUCH_SAMPLE];       //it indicate the state of each sample
    } touch_sample_t;

    typedef enum
    {
        TOUCH_STATE_RELEASE = 0, //when sensor not touched
        TOUCH_STATE_PUSH = 1,    //when sensor touched
        TOUCH_STATE_ABNORMAL = 2 //when the raw_sample of a sensor is too big!
    } touch_type_t;

    typedef struct
    {
        uint16_t hold_cnt[TOUCH_NUM];
        bool release_state[TOUCH_NUM];
    } touch_state_t;

    //Funcs
    void touch_init(void);
    void task_touch(void *pvParameter);
    void find_offset(uint32_t *touch_bench);

#ifdef __cplusplus
}
#endif
#endif
