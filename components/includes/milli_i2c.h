#pragma once

#ifndef milli_i2c_h
#define milli_i2c_h

#ifdef __cplusplus
extern "C"
{
#endif

//Includes
#include <stdio.h>
#include "main.h"
#include "esp_log.h"
#include "driver/i2c.h"

//Defines
#define I2C_MASTER_SCL_IO 1        // GPIO number used for I2C master clock
#define I2C_MASTER_SDA_IO 2        // GPIO number used for I2C master data
#define I2C_MASTER_FREQ_HZ 400000  // I2C master clock frequency, MAX is 1 MHz
#define I2C_MASTER_TIMEOUT_MS 1000 // The timeout for i2c operations
#define SLAVE_ADDR 0x74            // The address of salve device
#define FRAME_NUMS 32              // Maximmu number of frames
#define FRAME_SIZE 18              // The size of each frame in Byte
#define FRAME_DELAY 10             // The gap between frames in mS
#define WRITE_BIT I2C_MASTER_WRITE // I2C master write bit
#define READ_BIT I2C_MASTER_READ   // I2C master read bit
#define ACK_CHECK_EN 0x1           // I2C master will check ack or not
#define ACK_VAL 0x0                // I2C ack value
#define NACK_VAL 0x1               // I2C nack value

    //Vars
    QueueHandle_t i2c_qu;
    typedef struct
    {
        uint8_t data[FRAME_SIZE * FRAME_NUMS];
        bool delay;
    } frame_t;

    //Funcs
    esp_err_t i2c_master_init(void);
    esp_err_t i2c_master_write_slave(uint8_t *data, size_t size);
    void task_i2c_send(void *pvargs);
    void task_i2c_receive(void *pvarg);

#ifdef __cplusplus
}
#endif
#endif
