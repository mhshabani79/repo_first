#pragma once

#ifndef milli_ota_h
#define milli_ota_h

#ifdef __cplusplus
extern "C"
{
#endif

//Includes
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "nvs.h"
#include "nvs_flash.h"

//Defines
#define BUFFSIZE 4096
#define OTA_ACK 0xFD
#define PRINT_OTA_DATA 0 //set it to 1, if you wanna print the ota received bytes.

    //Vars
    typedef struct
    {
        uint32_t binary_file_length;
        bool image_header_was_checked;
        // char ota_write_data[BUFFSIZE];
        esp_ota_handle_t update_handle;
        esp_partition_t *update_partition;
        esp_partition_t *configured;
        esp_partition_t *running;
        struct
        {
            bool ready_new;
            uint32_t size;
            bool complete;
        } firmware;
    } ota_vars_t;
    static const char *OTA_TAG = "OTA";

    //Funcs
    void ota_delete(void);
    void ota_make_handlers(void);
    void ota_function(char ota_write_data[BUFFSIZE], uint16_t);

#ifdef __cplusplus
}
#endif
#endif