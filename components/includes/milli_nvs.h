#pragma once

#ifndef milli_nvs_h
#define milli_nvs_h

#ifdef __cplusplus
extern "C"
{
#endif
//Includes
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_log.h"


//Defines
#define NVS_VAR1 200
#define NVS_VAR2 1400
#define NVS_VAR3 123456789
#define NVS_VAR4 -1400
#define NVS_VAR5 "MilliGrade.ir"

//Vars
nvs_handle_t handle1;

typedef struct
{
    uint8_t var1;
    uint16_t var2;
    uint32_t var3;
    int32_t var4;
    char var5[64];
} nvs_vars_t;

//Funcs
void nvs_init(void);
nvs_vars_t nvs_get_data(void);
void nvs_update(nvs_vars_t);

#ifdef __cplusplus
}
#endif
#endif