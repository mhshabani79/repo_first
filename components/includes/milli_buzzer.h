#pragma once

#ifndef buzzer_h
#define buzzer_h

#ifdef __cplusplus
extern "C"
{
#endif

/**
* @note: This Peripheral is dependant from esp32 version.
*/

//INCLUDEs
#include <stdio.h>
#include "main.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "sdkconfig.h"
#include "esp_log.h"

    //FUNCs
    void buzzer_init(void);
    void make_sound(uint16_t note[][2]);
    void task_buzzer_receive(void *pvParameters);

//DEFINEs
#define LEDC_TIMER LEDC_TIMER_0
#define LEDC_MODE LEDC_LOW_SPEED_MODE
#define LEDC_OUTPUT_IO (5) // Define the output GPIO
#define LEDC_CHANNEL LEDC_CHANNEL_0
#define LEDC_DUTY_RES LEDC_TIMER_13_BIT // Set duty resolution to 13 bits
#define LEDC_DUTY (4095)                // Set duty to 50%. ((2 ** 13) - 1) * 50% = 4095
#define LEDC_FREQUENCY (3000)           // Frequency in Hertz. Set frequency at 5 kHz

    //VARs
    QueueHandle_t sound_qu;

    typedef struct
    {
        uint16_t sound1[9][2];
        uint16_t sound2[9][2];
        uint16_t sound3[9][2];
    } buzzer_sounds_t;

#ifdef __cplusplus
}
#endif
#endif /* buzzer_h */