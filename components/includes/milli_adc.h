#pragma once

#ifndef milli_adc_h
#define milli_adc_h

#ifdef __cplusplus
extern "C"
{
#endif

//includes
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "sdkconfig.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "driver/adc.h"

//defines
#define BRAND2 BRAND
#define TIMES 32                                                                          /*ADC buffer in Bytes*/
#define CH_SIZE (TIMES / ((ADC_RESULT_BYTE) * (sizeof(CHANNEL) / sizeof(adc_channel_t)))) /*each ldr or ntc size base on TIMES*/
#define GET_UNIT(x) ((x >> 3) & 0x1)
#if CONFIG_IDF_TARGET_ESP32
#define ADC_RESULT_BYTE 2
#define ADC_CONV_LIMIT_EN 1                  //For ESP32, this should always be set to 1
#define ADC_CONV_MODE ADC_CONV_SINGLE_UNIT_1 //ESP32 only supports ADC1 DMA mode
#define ADC_OUTPUT_TYPE ADC_DIGI_OUTPUT_FORMAT_TYPE1
#elif CONFIG_IDF_TARGET_ESP32S2
#define ADC_RESULT_BYTE 2
#define ADC_CONV_LIMIT_EN 0                          //??
#define ADC_CONV_MODE ADC_CONV_SINGLE_UNIT_2         //ADC_CONV_BOTH_UNIT
#define ADC_OUTPUT_TYPE ADC_DIGI_OUTPUT_FORMAT_TYPE1 //ADC_DIGI_OUTPUT_FORMAT_TYPE2
#elif CONFIG_IDF_TARGET_ESP32C3 || CONFIG_IDF_TARGET_ESP32H2
#define ADC_RESULT_BYTE 4
#define ADC_CONV_LIMIT_EN 0
#define ADC_CONV_MODE ADC_CONV_ALTER_UNIT //ESP32C3 only supports alter mode
#define ADC_OUTPUT_TYPE ADC_DIGI_OUTPUT_FORMAT_TYPE2
#elif CONFIG_IDF_TARGET_ESP32S3
#define ADC_RESULT_BYTE 4
#define ADC_CONV_LIMIT_EN 0
#define ADC_CONV_MODE ADC_CONV_BOTH_UNIT
#define ADC_OUTPUT_TYPE ADC_DIGI_OUTPUT_FORMAT_TYPE2
#endif

///////////////////////////////// the adc units and channels may differ in esp32 versions
#if CONFIG_IDF_TARGET_ESP32C3 || CONFIG_IDF_TARGET_ESP32S3 || CONFIG_IDF_TARGET_ESP32H2
#define ADC1_CHAN_MASK BIT(2) | BIT(3)
#define ADC2_CHAN_MASK BIT(0)
#define CHANNEL \
    (adc_channel_t[]) { ADC1_CHANNEL_2, ADC1_CHANNEL_3, (ADC2_CHANNEL_0 | 1 << 3) }
#endif
#if CONFIG_IDF_TARGET_ESP32S2
#define ADC1_CHAN_MASK NULL
#define ADC2_CHAN_MASK BIT(2) | BIT(3)
#define CHANNEL \
    (adc_channel_t[]) { (ADC2_CHANNEL_2 | 1 << 3), (ADC2_CHANNEL_3 | 1 << 3) }
#endif
#if CONFIG_IDF_TARGET_ESP32
#define ADC1_CHAN_MASK BIT(7)
#define ADC2_CHAN_MASK 0;
#define CHANNEL \
    (adc_channel_t[]) { ADC1_CHANNEL_7 }
#endif

    /////////////////////////////////FUNCs
    void continuous_adc_init(void);
    void task_adc_NTC_LDR(void *pvArg);
    void bubble_sort(uint16_t *arr, uint8_t len);

#ifdef __cplusplus
}
#endif
#endif