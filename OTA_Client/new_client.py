import socket
import os
import time
from timeit import default_timer as timer
from datetime import timedelta

######################################### MACROS
HOST = '192.168.1.6'
PORT = 3344
BUFF_SIZE = 4096
FILE_NAME = 'wifi_program.bin'
OTA_ACK = b'\xFD'
#########################################

buff = b''
err = 0
file_size = os.path.getsize(FILE_NAME)
packet_num = int(file_size / BUFF_SIZE)
f = open(FILE_NAME, 'rb')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
print('Connected to {}//{}'.format(HOST, PORT))
time.sleep(5)

while 1:
    msg = 'NEWFW,' + str(file_size) + ' '
    s.sendall(msg.encode())
    data = s.recv(1)
    if data == OTA_ACK:
        print('the Start ack received')
    else:
        print('WRONG Start ack')
        err = 1
        break

    t0 = timer()
    for i in range(packet_num + 1):
        if i == packet_num:
            if file_size % BUFF_SIZE != 0:
                print('Sending packet #{}'.format(i + 1))
                s.sendall(f.read(file_size % BUFF_SIZE))
            else:
                break
        else:
            print('Sending packet #{}'.format(i + 1))
            s.sendall(f.read(BUFF_SIZE))
        data = s.recv(1)
        if data == OTA_ACK:
            print('ack #{} received'.format(i + 1))
        else:
            print('WRONG ack [{}]!'.format(data))
            err = 1
            break
    t1 = timer()

    if not err:
        s.sendall('ENDOTA'.encode())
        data = s.recv(1)
        if data == OTA_ACK:
            print('Finish ack received')
        else:
            print('WRONG Finish ack [{}]!'.format(data))
            err = 1
            break
        print('OTA File Sent completely')
        print('OTA process time : {}'.format(timedelta(seconds=t1 - t0)))
        break

print('END.')
s.close()
